import React from 'react'
import { parseISO, format } from 'date-fns'
import { fr } from 'date-fns/locale'
import PropTypes from 'prop-types'

const Date = ({ dateString }) => {
    const date = parseISO(dateString)
    return (
        <time dateTime={dateString} className="text-gray-400">
            {format(date, 'd LLLL yyyy', { locale: fr })}
        </time>
    )
}

export default Date

Date.propTypes = {
    dateString: PropTypes.string,
}