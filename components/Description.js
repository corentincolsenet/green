import React from 'react'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import PropTypes from 'prop-types'

const Description = () => {
    const { t } = useTranslation('common')

    return (
        <div className="relative">
            <div className="flex flex-row">
                <div className="hidden sm:block left-0 w-2/3 relative -mt-36 -ml-14">
                    <Image
                        src={'/images/background-l.svg'}
                        width={1326}
                        height={1070}
                        layout="responsive"

                        alt={t("Image de fond")}
                    />
                </div>
                <div className="hidden sm:block right-0 w-3/4 absolute -mt-36">
                    <Image
                        src={'/images/background-r.svg'}
                        width={1277}
                        height={916}
                        layout="responsive"
                        alt={t("Image de fond")}
                    />
                </div>
                <div className="sm:hidden left-0 w-full relative -mt-36">
                    <Image
                        src={'/images/background-l-mobile.svg'}
                        width={610}
                        height={1012}
                        layout="responsive"

                        alt={t("Image de fond")}
                    />
                </div>
                <div className="sm:hidden right-0 w-full absolute -mt-36">
                    <Image
                        src={'/images/background-r-mobile.svg'}
                        width={391}
                        height={650}
                        layout="responsive"
                        alt={t("Image de fond")}
                    />
                </div>
            </div>
            <div className="sm:mx-auto sm:max-w-6xl">
                 <div className="absolute items-center flex flex-col z-40 top-4 md:top-10 lg:top-20 sm:left-32 md:left-52 space-y-6">
                    <p className="text-center font-brandon text-5xl sm:text-xl md:text-3xl lg:text-5xl md:w-60 lg:w-96 font-medium text-green-g">
                        {t("Jardiner comme jamais auparavant !")}
                    </p>
                    <p className="text-center font-brandon sm:text-md md:text-lg mx-6 sm:mx-0 text-xl sm:w-96 font-light text-black">
                        {t("Solution d'aide à la sélection de végétaux et à la plantation ainsi qu'à la prévisualisation de végétaux en réalité augmentée pour les professionnels du paysage et leurs clients")}
                    </p>
                    <a href="#" className="text-center text-white bg-gradient-to-r from-green-g to-green-700 rounded-full p-3 text-sm md:text-lg font-brandon font-medium shadow-md w-44">
                        {t('En apprendre plus')}
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Description

Description.propTypes = {}