import React from 'react'
import { useTranslation } from 'next-i18next'
import PropTypes from 'prop-types'

const Features = () => {
    const { t } = useTranslation('common')

    return (
        <div className="relative mx-auto max-w-5xl mt-4">
            <h1 id="features" className="w-full text-center font-aleo text-5xl">{t("Fonctionnalités")}</h1>
            <p className="w-full font-brandon text-center text-3xl mt-6">{t("Voici quelques images de l'application Green")}</p>
            <div className="flex flex-col sm:flex-row justify-center items-center space-y-8 sm:space-x-16 mt-10 w-full">
                <img src={'/images/first-screen.png'} alt="Première capture" width="200" height="250" />
                <img src={'/images/second-screen.jpg'} alt="Seconde capture" width="200" height="250" />
            </div>
        </div>
    )
}

export default Features

Features.propTypes = {}