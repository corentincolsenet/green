import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

import FacebookLink from '@/components/SocialLinks/FacebookLink'
import InstagramLink from '@/components/SocialLinks/InstagramLink'
import LinkedInLink from '@/components/SocialLinks/LinkedInLink'

const Footer = () => {
    const { t } = useTranslation('common')

    return (
        <footer className="bg-gradient-to-b from-green-g to-green-400" style={{zIndex: 999}}>
            <div className="pt-8 pb-4">

                <div className="flex justify-center mt-8 mx-4 sm:my-8 sm:mx-12 md:mx-20 lg:mx-28 xl:mx-36">
                    <div className="h-24 w-60 lg:h-52 lg:w-96 relative">
                        <Image
                            src={'/images/logoFull.svg'}
                            layout="fill"
                            objectFit="contain"
                            alt={t("Logo Green")}
                        />
                    </div>
                </div>


                <div className="flex justify-center space-x-3 px-8 mt-10 sm:space-x-8 sm:px-12 md:space-x-12 md:px-20 lg:mb-12 lg:px-28 xl:px-36 xl:space-x-16">

                    <FacebookLink size="14" />
                    <InstagramLink size="14" />
                    <LinkedInLink size="14" />

                </div>

                <div className="text-center text-sm font-brandon">
                    <p className="text-black mb-1">{t('Fait par Green')}</p>
                </div>

            </div>
        </footer>
    )
}

export default Footer