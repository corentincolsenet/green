import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import { useRouter } from 'next/router'
import resolveConfig from 'tailwindcss/resolveConfig'

import tailwindConfig from '../tailwind.config'

const fullConfig = resolveConfig(tailwindConfig)
const breakpointSm = parseInt(fullConfig.theme.screens.sm.replace('px', ''))

function useWindowSize() {
    const [size, setSize] = useState({width: 0, height: 0})
    useEffect(() => {
        function updateSize() {
            setSize({width: window.innerWidth, height: window.innerHeight})
        }
        window.addEventListener('resize', updateSize)
        updateSize()
        return () => window.removeEventListener('resize', updateSize)
    }, [])
    return size
}

const Header = () => {
    const { t, i18n } = useTranslation('common')
    const [showLocalesOnDesktop, setShowLocalesOnDesktop] = useState(false)
    const [showLocalesOnPhone, setShowLocalesOnPhone] = useState(false)
    const [showHeader, setShowHeader] = useState(false)
    const { width, height } = useWindowSize()
    const router = useRouter()

    const flags = {
        fr: "🇫🇷",
        en: "🇬🇧"
    }

    if (width >= breakpointSm && showHeader === true) {
        setShowHeader(false)
    }    

    return (
        <header>
            <nav className={`fixed top-0 inset-0 w-2/3 ${!showHeader ? 'bg-transparent relative' : 'bg-white fixed'} sm:bg-transparent sm:block sm:w-full z-50`}>
                <div className="max-w-6xl mx-auto px-2 sm:px-6 lg:px-8">
                    <div className="sm:relative flex flex-row-reverse sm:flex-row items-center justify-between h-36">
                        {showHeader &&
                            <div className="relative sm:hidden flex items-center mr-3 sm:static sm:ml-6 sm:mr-0">
                                <div className="block h-8 w-8 mx-3 cursor-pointer text-2xl" onClick={() => setShowLocalesOnPhone(!showLocalesOnPhone)}>
                                    {flags[i18n.language]}
                                </div>

                                {showLocalesOnPhone &&
                                    <div className="absolute flex flex-col top-4 w-12 py-3 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="select-locale">
                                        {router.locales.map(l => (
                                            <Link key={l} href="/" locale={l}>
                                                <a className="mx-3 text-2xl" role="menuitem">{flags[l]}</a>
                                            </Link>
                                        ))}
                                    </div>
                                }
                            </div>
                        }

                        {!showHeader &&
                            <div className="flex-1 flex sm:items-center sm:justify-center">
                                <div className="hidden sm:block h-96 w-96 absolute left-1/2 bottom-1/3 transform -translate-x-1/2">
                                    <Image
                                        src={'/images/circle.svg'}
                                        layout="fill"
                                        objectFit="contain"
                                        alt={t("Cercle")}
                                    />
                                </div>
                                <div className="flex-shrink-0 flex items-center">
                                    <div className="hidden sm:block md:block lg:hidden h-32 w-32 relative">
                                        <Image
                                            src={'/images/logo.svg'}
                                            layout="fill"
                                            objectFit="contain"
                                            alt={t("Logo Green")}
                                        />
                                    </div>
                                    
                                    <div className="block sm:hidden left-1/2 lg:left-0 bottom-0 lg:mx-4 lg:mt-2 sm:mx-0 sm:mt-0 lg:block space-x-8 h-24 w-32 lg:h-32 lg:w-56 relative">
                                        <Image
                                            src={'/images/logoFull.svg'}
                                            layout="fill"
                                            objectFit="contain"
                                            alt={t("Logo Green")}
                                        />
                                    </div>
                                </div>

                                <div className="hidden sm:block sm:ml-6 z-40">
                                    <div className="flex space-x-4">
                                        <Link href="/">
                                            <a className="text-black p-4 text-xs lg:text-sm font-aleo font-light hover:text-green-g">{t('Menu')}</a>
                                        </Link>
                                        <Link href="/">
                                            <a className="text-black p-4 text-xs lg:text-sm font-aleo font-light hover:text-green-g">{t('À propos de nous')}</a>
                                        </Link>
                                        <Link href="/#features">
                                            <a className="text-black p-4 text-xs lg:text-sm font-aleo font-light hover:text-green-g">{t('Fonctionnalités')}</a>
                                        </Link>
                                        <Link href="/">
                                            <a className="text-black p-4 text-xs lg:text-sm font-aleo font-light hover:text-green-g">{t('Contactez-nous')}</a>
                                        </Link>
                                    </div>
                                </div>
                                <a href="#" className="hidden sm:block text-white bg-gradient-to-r from-green-g to-green-700 rounded-full p-3 text-xs lg:text-lg font-brandon font-medium shadow-md">{t('Télécharger Green')}</a>
                            </div>
                        }

                        <div className="hidden sm:block right-0 flex items-center pr-3 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                            <div className="hidden sm:block h-8 w-8 mx-3 cursor-pointer text-2xl" onClick={() => setShowLocalesOnDesktop(!showLocalesOnDesktop)}>
                                {flags[i18n.language]}
                            </div>

                            {showLocalesOnDesktop &&
                                <div className="absolute flex flex-col top-20 w-12 py-3 bg-transparent focus:outline-none" style={{zIndex: 1}} role="menu" aria-orientation="vertical" aria-labelledby="select-locale">
                                    {router.locales.map(l => (
                                        <Link key={l} href="/" locale={l}>
                                            <a className="mx-3 text-2xl" role="menuitem">{flags[l]}</a>
                                        </Link>
                                    ))}
                                </div>
                            }
                        </div>

                        <div className="relative sm:hidden inset-y-0 flex ml-3 items-center sm:hidden" onClick={() => setShowHeader(!showHeader)}>
                            {!showHeader &&
                                <button type="button" className="inline-flex items-center justify-center p-2 text-green-g focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                                    <span className="sr-only">{t('Ouvrir le menu principal')}</span>
                                    <svg className="block h-10 w-10" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                                    </svg>
                                    <svg className="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                                    </svg>
                                </button>
                            }
                            {showHeader &&
                                <button type="button" className="inline-flex items-center justify-center p-2 text-green-g focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                                    <svg className="block h-10 w-10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg>
                                </button>
                            }
                        </div>
                    </div>
                </div>

                <div className="sm:hidden" id="mobile-menu">
                    {showHeader &&
                        <div className="relative">
                            <div className="relative">
                                <div className="flex flex-col items-center space-y-6">
                                    <Link href="/">
                                        <a onClick={() => setShowHeader(!showHeader)} className="p-4 text-xl font-aleo font-bold text-green-g">{t('Menu')}</a>
                                    </Link>
                                    <Link href="/">
                                        <a onClick={() => setShowHeader(!showHeader)} className="p-4 text-xl font-aleo font-bold text-green-g">{t('À propos de nous')}</a>
                                    </Link>
                                    <Link href="/#features">
                                        <a onClick={() => setShowHeader(!showHeader)} className="p-4 text-xl font-aleo font-bold text-green-g">{t('Fonctionnalités')}</a>
                                    </Link>
                                    <Link href="/">
                                        <a onClick={() => setShowHeader(!showHeader)} className="p-4 text-xl font-aleo font-bold text-green-g">{t('Contactez-nous')}</a>
                                    </Link>

                                    <a href="#" className="text-white bg-gradient-to-r from-green-g to-green-700 rounded-full px-6 py-4 text-xl font-brandon font-light shadow-md">{t('Télécharger Green')}</a>
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </nav>
        </header>
    )
}

export default Header