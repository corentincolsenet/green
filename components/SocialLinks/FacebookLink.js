import PropTypes from 'prop-types'
import SocialLink from '@/components/SocialLinks/SocialLink'

const FacebookLink = ({ size = "8" }) => {
    return (
        <SocialLink
            href="https://www.facebook.com/DiscoverGreenEIP/"
            src="facebook.svg"
            alt="Facebook"
            size={size}
        />
    )
}

export default FacebookLink

FacebookLink.propTypes = {
    size: PropTypes.string,
}
