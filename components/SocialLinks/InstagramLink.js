import PropTypes from 'prop-types'
import SocialLink from '@/components/SocialLinks/SocialLink'

const InstagramLink = ({ size = "8" }) => {
    return (
        <SocialLink
            href="https://www.instagram.com/wearegreeen/"
            src="instagram.svg"
            alt="Instragram"
            size={size}
        />
    )
}

export default InstagramLink

InstagramLink.propTypes = {
    size: PropTypes.string,
}
