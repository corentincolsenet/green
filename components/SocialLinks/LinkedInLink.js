import PropTypes from 'prop-types'
import SocialLink from '@/components/SocialLinks/SocialLink'

const LinkedInLink = ({ size = "8" }) => {
    return (
        <SocialLink
            href="https://www.linkedin.com/company/discovergreen/"
            src="linkedin.svg"
            alt="LinkedIn"
            size={size}
        />
    )
}

export default LinkedInLink

LinkedInLink.propTypes = {
    size: PropTypes.string,
}
