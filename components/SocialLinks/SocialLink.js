import React from 'react'
import Image from 'next/image'
import PropTypes from 'prop-types'

const SocialLink = ({ href, src, alt, size }) => {

    return (
        <div className={`flex h-${size} w-${size} justify-center items-center md:h-12 md:w-12 lg:h-16 lg:w-16`}>
            <div className="h-3/5 w-3/5 relative" title={alt}>
                <a href={href} target="_blank">
                    <Image
                        src={`/images/${src}`}
                        layout="fill"
                        objectFit="contain"
                        alt={alt}
                    />
                </a>
            </div>
        </div>
    )
}

export default SocialLink

SocialLink.propTypes = {
    href: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    alt: PropTypes.string,
    size: PropTypes.string
}