import React from 'react'
import { useTranslation } from 'next-i18next'
import PropTypes from 'prop-types'

const Testimonials = () => {
    const { t } = useTranslation('common')

    return (
        <div className="relative mx-auto max-w-5xl mt-4">
            <h1 className="w-full text-center font-aleo text-5xl">{t("Témoignages")}</h1>
            <div className="responsive flex flex-col justify-between my-10 space-y-4">
                <p className="font-brandon text-3xl font-medium text-center">{t("Y'a t-il des solutions technologiques pour vous aider dans votre métier ?")}</p>
                <div className="flex flex-row justify-between mx-8 items-center list-disc">
                    <li className="font-brandon text-xl italic">ARIS Josian ({t("Paysagiste")})</li>
                    <p className="font-brandon text-xl ml-4">{t("\"La plupart des problèmes sont d'être en adéquation avec le client et d'en satisfaire ses envies.\"")}</p>
                </div>
                <p className="font-brandon text-3xl font-medium text-center list-disc">{t("Disposez-vous actuellement de technologies viable ?")}</p>
                <div className="flex flex-row justify-between mx-8 items-center">
                    <li className="font-brandon text-xl italic">MASSE Jean-Baptiste ({t("Paysagiste")})</li>
                    <p className="font-brandon text-xl ml-4">{t("\"On n'utilise pas de logiciel. On est sur du matériel physique tel que le papier et le stylo.\"")}</p>
                </div>
                <p className="font-brandon text-3xl font-medium text-center list-disc">{t("Quel est la fonctionnalité que vous préférez le plus chez Green et pourquoi ?")}</p>
                <div className="flex flex-row justify-between mx-8 items-center">
                    <li className="font-brandon text-xl italic">MASSE Jean-Baptiste ({t("Paysagiste")})</li>
                    <p className="font-brandon text-xl ml-4">{t("\"C'est le fait de pouvoir visualiser les végétaux sur le terrain car cela va permettre de pouvoir appréhender l'espace occupé par l'aménagement qui sera effectué.\"")}</p>
                </div>
            </div>
        </div>
    )
}

export default Testimonials

Testimonials.propTypes = {}