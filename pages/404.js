import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'

import Layout from '@/components/Layout'

const Custom404 = () => {
    const { t } = useTranslation('common')
    
    return (
        <Layout>
            <h1>{t('Page introuvable 😱')}</h1>
        </Layout>
    )
}

export default Custom404

export async function getStaticProps({ locale }) {
    return {
        props: {
            ...await serverSideTranslations(locale, ['common']),
        },
    }
}