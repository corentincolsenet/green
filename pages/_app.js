import { SWRConfig } from 'swr'
import { appWithTranslation } from 'next-i18next'

import fetch from '@/lib/fetchJson'
import '../styles/globals.scss'
import '../styles/fonts.scss'

const MyApp = ({ Component, pageProps }) => (
    <SWRConfig
        value={{
            fetcher: fetch,
            onError: (err) => {
                console.error(err)
            },
        }}
    >
        <Component {...pageProps} />
    </SWRConfig>
)

export default appWithTranslation(MyApp)
