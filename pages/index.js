import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { NextSeo } from 'next-seo'
import { useTranslation } from 'next-i18next'

import Layout from '@/components/Layout'
import Description from '@/components/Description'
import Features from '@/components/Features'
import Testimonials from '@/components/Testimonials'

const Home = () => {
  const { t } = useTranslation('common')
  return (
    <Layout>
      <NextSeo
        title="Green"
        description="Green réinvente"
      />
        <Description />
        <Features />
        <Testimonials />
    </Layout>
  )
}

export default Home

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common']),
    },
  }
}