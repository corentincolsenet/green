import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'

import Layout from '@/components/Layout'

const Home = () => {
    const router = useRouter()

    return (
        <Layout>
            Pro
        </Layout>
    )
}

export default Home

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...await serverSideTranslations(locale, ['common', 'pro']),
    },
  }
}