const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './pages/**/*.{js,ts,jsx,tsx}',
        './components/**/*.{js,ts,jsx,tsx}'
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
        colors: {
            ...colors,
            'green-g' : {
                DEFAULT: '#adcc3a'
            },
        },
        fontFamily: {
            'aleo' : ['Aleo'],
            'brandon' : ['Brandon']
        },
        boxShadow: {
            sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
            'sm-solid': '0 1px 2px 0 rgba(0, 0, 0, 0.56)',
            DEFAULT: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
            solid: '0 1px 2px 0 rgba(0, 0, 0, 0.3), 0 1px 2px 0 rgba(0, 0, 0, 0.3)',
            md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
            'md-solid': '0 4px 0px 0px rgba(0, 0, 0, 0.5), 0 2px 0px 0px rgba(0, 0, 0, 0.5)',
            lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
            'lg-solid': '0 10px 0px 0px rgba(0, 0, 0, 0.8), 0 4px 0px 0px rgba(0, 0, 0, 0.8)',
            xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
            'xl-solid': '0 20px 0px 0px rgb(0, 0, 0, 1), 0 10px 0px 0px rgb(0, 0, 0, 0.8)',
            '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
            '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
            inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
            none: 'none',
        },
        screens: {
            'vsm': '410px',
            'sm': '640px',
            'md': '768px',
            'lg': '1024px',
            'xl': '1280px'
        },
    },
    variants: {
        extend: {
            borderWidth: ['hover', 'focus']
        },
    },
    plugins: [],
}
